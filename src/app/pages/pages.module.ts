import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PatientComponent } from './patient/patient.component';
import { CreatepatientComponent } from './createpatient/createpatient.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
    ],
    declarations: [
        PatientComponent,
        CreatepatientComponent,
        UpdatePatientComponent
    ]
})
export class PagesModule { }