import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { AppServicesService } from "./../../shared/service/app-services.service";

@Component({
  selector: "app-update-patient",
  templateUrl: "./update-patient.component.html",
  styleUrls: ["./update-patient.component.css"],
})
export class UpdatePatientComponent implements OnInit {
  mainForm: FormGroup;
  data: string = "";
  noData: string = "";
  showLoader: boolean = false;

  constructor(
    private fb: FormBuilder,
    private apiService: AppServicesService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.mainForm = this.fb.group({
      name: ["", [Validators.required]],

      phone: ["", [Validators.required]],

      gender: ["", [Validators.required]],
      email: ["", [Validators.required]],
      patientid: ["", [Validators.required]],
      birthDate: ["", [Validators.required]],
    });
  }

  submitForm() {
    this.data = "";
    this.noData = "";
    let url;
    console.log("PostData---", this.mainForm.value);

    url = `/api/patients/cerner/update/${this.mainForm.value.patientid}`;
    this.apiService.patch(url, this.mainForm.value).subscribe(
      (response: any) => {
        if (response && response.status === 200) {
          if (response.data && response.data.issue) {
            this.noData = response.data.issue[0].diagnostics;
          } else {
            console.log("response.data---", response);
            this.data = response;
            this.showLoader = false;
          }
        }
      },
      (errorResult) => {
        console.log("errorResult---", errorResult);
      }
    );
  }
}
