import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AppServicesService } from "./../../shared/service/app-services.service";

@Component({
  selector: "app-patient",
  templateUrl: "./patient.component.html",
  styleUrls: ["./patient.component.scss"],
})
export class PatientComponent implements OnInit {
  mainForm: FormGroup;
  data: any = [];
  noData: string = "";
  showLoader: boolean = false;
  type: any;
  patientData: any;
  showUpdateBtn: boolean = false;

  constructor(
    private fb: FormBuilder,
    private apiService: AppServicesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.mainForm = this.fb.group({
      parentid: ["", [Validators.required]],
      type: ["", [Validators.required]],
    });
  }
  submitForm() {
    this.showLoader = true;
    this.data = [];
    this.noData = "";
    //geting data from apis...
    const id = this.mainForm.value.parentid;
    let url;
    this.type = this.mainForm.value.type;
    if (this.mainForm.value.type == "cerner") {
      //if type is cerner...
      url = "/api/patients/cerner/";
    } else {
      //if type is epic...
      url = "/api/patients/";
    }
    this.apiService.get(url, id).subscribe(
      (response: any) => {
        if (response && response.status === 200) {
          if (response.data && response.data.issue) {
            this.noData = response.data.issue[0].diagnostics;
            this.showLoader = false;
          } else {
            this.data = response.data;
            this.showUpdateBtn = true;

            this.showLoader = false;
          }
        }
      },
      (errorResult) => {
        console.log("errorResult---", errorResult);
      }
    );
  }
}
