import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { AppServicesService } from "./../../shared/service/app-services.service";

@Component({
  selector: "app-createpatient",
  templateUrl: "./createpatient.component.html",
  styleUrls: ["./createpatient.component.scss"],
})
export class CreatepatientComponent implements OnInit {
  mainForm: FormGroup;
  data: string = "";
  noData: string = "";
  showLoader: boolean = false;

  constructor(
    private fb: FormBuilder,
    private apiService: AppServicesService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.mainForm = this.fb.group({
      name: ["", [Validators.required]],
      type: ["", [Validators.required]],
      phone: ["", [Validators.required]],
      email: ["", [Validators.required]],
      gender: ["", [Validators.required]],
      birthDate: ["", [Validators.required]],
      line: ["", [Validators.required]],
      city: ["", [Validators.required]],
      state: ["", [Validators.required]],
      postalCode: ["", [Validators.required]],
      country: ["", [Validators.required]],
      maritalStatus: ["", [Validators.required]],
    });
  }

  submitForm() {
    this.data = "";
    this.noData = "";
    let url;

    if (this.mainForm.value.type == "cerner") {
      //if type is cerner ..
      url = "/api/patients/cerner";
    } else {
      //if type is epic ..
      url = "/api/patients/";
    }

    this.apiService.post(url, this.mainForm.value).subscribe(
      (response: any) => {
        if (response && response.status === 200) {
          if (response.data && response.data.issue) {
            this.noData = response.data.issue[0].diagnostics;
          } else {
            this.data = response.data;
            this.showLoader = false;
          }
        }
      },
      (errorResult) => {
        console.log("errorResult---", errorResult);
      }
    );
  }
}
